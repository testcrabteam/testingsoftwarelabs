﻿/*
    Проект является собственностью разработчика известного как Mister Class.
    Распространение исходных файлов должно происходить только с его разрешения.
    Ибо нечего тырить чужие исходники, надо которыми он старается.
    Попытки стырить исходники карается штрафом, от злобного взгляда до пинка.
    Mister Class © 2019 All rights reserved.
    Task: Тестирование класса StringHandler модульным тестом
 */
// <copyright file="Program.cs" company="No">
// Copyright (c) 2019 All Rights Reserved
// </copyright>
// <author>Mister Class</author>
// <date>10/06/2019 19:59:21 </date>
// <summary>Main Program</summary>


using System;
using System.Text;
using System.IO;

namespace Test
{
    class Program
    {

        class StringHandler
        {
            /// <summary>
            /// Removing one symbol from string
            /// </summary>
            /// <param name="builder">StringBuilder for handling</param>
            /// <param name="index">Index symbol to be removed</param>
            /// <returns></returns>
            public static string SymbRemoveFromString(StringBuilder builder, int index)
            {
                if (builder != null && index >= 0 && index < builder.Length)
                {
                    builder.Remove(index, 1);
                    return builder.ToString();
                }
                return "-1";
            }

            /// <summary>
            /// Игнорирование пробелов с определенного номера индекса
            /// </summary>
            /// <param name="str">Обрабатываемая строка</param>
            /// <param name="startInd">Индекс старта обработки</param>
            /// <returns>Индекс первого непробельного символа</returns>
            public static int StrSpaceIgnore(string str, int startInd)
            {
                if (str != null)
                {
                    int i = startInd;
                    for (; i < str.Length && str[i] == ' '; i++) ;
                    return i;
                }
                return startInd;
            }
            /// <summary>
            /// Свап двух соседних слов в строке
            /// </summary>
            /// <param name="str">Обрабатываемая строка</param>
            /// <returns>Конечная строка</returns>
            public static string StrWordsSwap(string str)
            {
                string res = str;
                if (str != null)
                {
                    string str1, str2;
                    int i = 0;
                    int startInd1, startInd2;
                    while (i < str.Length)
                    {
                        str1 = "";
                        str2 = "";
                        i = StrSpaceIgnore(str, i);     //Ignore begin spacers
                        startInd1 = i;
                        while (i < str.Length && str[i] != ' ')
                        {
                            str1 = str1.Insert(str1.Length, str[i++].ToString());       //First word save
                        }
                        i = StrSpaceIgnore(str, i);
                        startInd2 = i;
                        while (i < str.Length && str[i] != ' ')
                        {
                            str2 = str2.Insert(str2.Length, str[i++].ToString());        //Second word save
                        }
                        if (str1 != "" && str2 != "")
                        {
                            res = res.Remove(startInd1, str1.Length);                   //Insert second word instead first
                            res = res.Insert(startInd1, str2);
                            startInd2 = startInd2 - str1.Length + str2.Length;          //Calculate start index of second word
                            res = res.Remove(startInd2, str2.Length);                   //Insert first word instead second
                            res = res.Insert(startInd2, str1);
                        }
                    }
                }
                return res;
            }
        }

        class FileHandler
        {
            /// <summary>
            /// Render on the screen text from file with swapped neighboring words
            /// </summary>
            /// <param name="reader">File for reading</param>
            public static string FileTextHandling(string reader, string writer)
            {
                if (reader != null && writer != null)
                {
                    StreamReader read = new StreamReader(reader);
                    string res = read.ReadToEnd();
                    res = StringHandler.StrWordsSwap(res);
                    StreamWriter write = new StreamWriter(writer);
                    write.WriteLine(res);
                    write.Close();
                    read.Close();
                    return res;
                }
                return "Error in Handling!";
            }
        }
        /// <summary>
        /// Log class
        /// </summary>
        class Log
        {
            private static StreamWriter log = new StreamWriter("../../../log.txt");
            /// <summary>
            /// Log message in file
            /// </summary>
            /// <param name="str"></param>
            public static void Add(string str)
            {
                log.WriteLine(str);
            }
            /// <summary>
            /// Close file
            /// </summary>
            public static void Close()
            {
                log.Close();
            }
        }
        /// <summary>
        /// String Handler tester
        /// </summary>
        class FileHandlerTester : ITest
        {
            public FileHandlerTester()
            {
                Run();
            }

            private void Run()
            {
                FileTextHandlingTest1();
            }
            /// <summary>
            /// Logger
            /// </summary>
            /// <param name="message"></param>
            public void LogMessage(string message)
            {
                Log.Add(message);
            }
            /// <summary>
            /// Test method FileTextHandling
            /// </summary>
            public void FileTextHandlingTest1()
            {
                /*StreamReader[] readers = new StreamReader[]
                {
                    new StreamReader("../../../Read/Test1.txt"),
                    new StreamReader("../../../Read/Test2.txt"),
                    new StreamReader("../../../Read/Test3.txt"),
                    new StreamReader("../../../Read/Test4.txt"),
                    new StreamReader("../../../Read/Test5.txt"),
                    new StreamReader("../../../Read/Test6.txt"),
                };*/
                string[] writers = new string[]
                {
                    "../../../Write/Test1.txt",
                    "../../../Write/Test2.txt",
                    "../../../Write/Test3.txt",
                    "../../../Write/Test4.txt",
                    "../../../Write/Test5.txt",
                    "../../../Write/Test6.txt",
                };

                string[] readers = new string[]
                {
                    "../../../Read/Test1.txt",
                    "../../../Read/Test2.txt",
                    "../../../Read/Test3.txt",
                    "../../../Read/Test4.txt",
                    "../../../Read/Test5.txt",
                    "../../../Read/Test6.txt",
                };

                for (int i = 0; i < readers.Length; i++)
                {
                    string res = FileHandler.FileTextHandling(readers[i], writers[i]);
                    LogMessage(res);
                }

            }
        }

        static void Main(string[] args)
        {
            FileHandlerTester tester = new FileHandlerTester();
            Log.Close();
        }
    }
}
