﻿/*
    Проект является собственностью разработчика известного как Mister Class.
    Распространение исходных файлов должно происходить только с его разрешения.
    Ибо нечего тырить чужие исходники, надо которыми он старается.
    Попытки стырить исходники карается штрафом, от злобного взгляда до пинка.
    Mister Class © 2019 All rights reserved.
    Task: Программа, которая считывает текст из файла и выводит его на экран, меняя местами каждые два соседних слова. 
 */
// <copyright file="Program.cs" company="No">
// Copyright (c) 2019 All Rights Reserved
// </copyright>
// <author>Mister Class</author>
// <date>10/03/2019 23:00:00 </date>
// <summary>Main Program</summary>

using System;
using System.IO;

namespace Lab1_3
{
    class Program
    {
        /// <summary>
        /// Игнорирование пробелов с определенного номера индекса
        /// </summary>
        /// <param name="str">Обрабатываемая строка</param>
        /// <param name="startInd">Индекс старта обработки</param>
        /// <returns>Индекс первого непробельного символа</returns>
        public static int StrSpaceIgnore(string str, int startInd)
        {
            if (str != null)
            {
                int i = startInd;
                for (; i < str.Length && str[i] == ' '; i++) ;
                return i;
            }
            return startInd;
        }
        /// <summary>
        /// Свап двух соседних слов в строке
        /// </summary>
        /// <param name="str">Обрабатываемая строка</param>
        /// <returns>Конечная строка</returns>
        public static string StrWordsSwap(string str)
        {
            string res = str;
            if (str != null)
            {        
                string str1, str2;
                int i = 0;
                int startInd1, startInd2;
                while (i < str.Length)
                {
                    str1 = "";
                    str2 = "";
                    i = StrSpaceIgnore(str, i);     //Ignore begin spacers
                    startInd1 = i;
                    while (i < str.Length && str[i] != ' ')
                    {
                        str1 = str1.Insert(str1.Length, str[i++].ToString());       //First word save
                    }
                    i = StrSpaceIgnore(str, i);
                    startInd2 = i;
                    while (i < str.Length && str[i] != ' ')
                    {
                       str2 = str2.Insert(str2.Length, str[i++].ToString());        //Second word save
                    }
                    if (str1 != "" && str2 != "")
                    {
                        res = res.Remove(startInd1, str1.Length);                   //Insert second word instead first
                        res = res.Insert(startInd1, str2);
                        startInd2 = startInd2 - str1.Length + str2.Length;          //Calculate start index of second word
                        res = res.Remove(startInd2, str2.Length);                   //Insert first word instead second
                        res = res.Insert(startInd2, str1);
                    }
                }
            }
            return res;
        }
        /// <summary>
        /// Render on the screen text from file with swapped neighboring words
        /// </summary>
        /// <param name="reader">File for reading</param>
        public static void FileTextHandling(StreamReader reader)
        {
            if (reader != null)
            {
                string res = reader.ReadToEnd();
                Console.WriteLine("Original text: ");
                Console.WriteLine(res);
                res = StrWordsSwap(res);
                Console.WriteLine("Result text: ");
                Console.WriteLine(res);
            }
        }
        public static string Test1()
        {
            return "Test1.txt";
        }
        public static string Test2()
        {
            return "Test2.txt";
        }
        public static string Test3()
        {
            return "Test3.txt";
        }
        public static string Test4()
        {
            return "Test4.txt";
        }
        public static string Test5()
        {
            return "Test5.txt";
        }
        public static string Test6()
        {
            return "Test6.txt";
        }
        static void Main(string[] args)
        {
            string file = Test1();
            string path = $"../../../Test/{file}";
            StreamReader reader = new StreamReader(path);
            Console.WriteLine($"Test 1. Handling of {file}");
            FileTextHandling(reader);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            file = Test2();
            path = $"../../../Test/{file}";
            reader = new StreamReader(path);
            Console.WriteLine($"Test 2. Handling of {file}");
            FileTextHandling(reader);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            file = Test3();
            path = $"../../../Test/{file}";
            reader = new StreamReader(path);
            Console.WriteLine($"Test 3. Handling of {file}");
            FileTextHandling(reader);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            file = Test4();
            path = $"../../../Test/{file}";
            reader = new StreamReader(path);
            Console.WriteLine($"Test 4. Handling of {file}");
            FileTextHandling(reader);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            file = Test5();
            path = $"../../../Test/{file}";
            reader = new StreamReader(path);
            Console.WriteLine($"Test 5. Handling of {file}");
            FileTextHandling(reader);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            file = Test6();
            path = $"../../../Test/{file}";
            reader = new StreamReader(path);
            Console.WriteLine($"Test 6. Handling of {file}");
            FileTextHandling(reader);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();
        }
    }
}
