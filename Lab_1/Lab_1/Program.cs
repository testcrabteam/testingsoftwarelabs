﻿/*
    Проект является собственностью разработчика известного как Mister Class.
    Распространение исходных файлов должно происходить только с его разрешения.
    Ибо нечего тырить чужие исходники, надо которыми он старается.
    Попытки стырить исходники карается штрафом, от злобного взгляда до пинка.
    Mister Class © 2019 All rights reserved.
    Task: Дана целочисленная квадратная матрица. 
          Определить сумму элементов в тех столбцах, которые не содержат отрицательных элементов. 
 */
// <copyright file="Program.cs" company="No">
// Copyright (c) 2019 All Rights Reserved
// </copyright>
// <author>Mister Class</author>
// <date>10/03/2019 23:00:00 </date>
// <summary>Main Program</summary>

using System;

namespace Lab_1
{
    class Program
    {
        /// <summary>
        /// Render the matrix on Console screen
        /// </summary>
        /// <param name="matrix">your matrix</param>
        public static void MatrixRender(int[,] matrix)
        {
            if (matrix != null)
            {
                for (int i = 0; i < matrix.GetUpperBound(0) + 1; i++)
                {
                    for (int j = 0; j < matrix.GetUpperBound(1) + 1; j++)
                    {
                        Console.Write("{0, 4:d}", matrix[i, j]);
                    }
                    Console.WriteLine();
                }
            }
        }
        /// <summary>
        /// Calculate sums of colums only with positive integers
        /// </summary>
        /// <param name="matrix">Handling matrix</param>
        /// <returns>Array sums of colums</returns>
        public static int[] MatrixPositiveColumnSum(int[,] matrix)
        {
            int rows = matrix.GetUpperBound(0) + 1;
            int cols = matrix.GetUpperBound(1) + 1;
            int[] sum = new int[cols];
            int j;
            if (rows > 0)
            {
                for (int i = 0; i < cols; i++)
                {
                    j = 0;
                    for(; j < rows; j++)
                    {
                        if (matrix[j, i] < 0) break;
                    }
                    if (j == cols)
                        for (int a = 0; a < rows; a++) sum[i] += matrix[a, i];
                }
            }
            return sum;
        }

        public static int[,] Test1()
        {
            return new int[1, 1] { { -1 } };
        }

        public static int[,] Test2()
        {
            return new int[1, 1] { { 1 } };
        }
        public static int[,] Test3()
        {
            return new int[3, 3] { { 1, 2, 3 }, { 4, 5, 3 }, { 1, 6, 3 } };
        }
        public static int[,] Test4()
        {
            return new int[3, 3] { { -1, 2, 3 }, { 4, -5, 3 }, { 1, 6, -3 } };
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Test 1:");
            Console.WriteLine("Your matrix:");
            int[,] mat = Test1();
            MatrixRender(mat);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();
            int[] res = MatrixPositiveColumnSum(mat);
            foreach (int el in res)
            {
                Console.Write("{0, 4:d}", el);
            }
            Console.WriteLine();
            Console.ReadKey(false);

            Console.WriteLine("Test 2:");
            Console.WriteLine("Your matrix:");
            mat = Test2();
            MatrixRender(mat);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();
            res = MatrixPositiveColumnSum(mat);
            foreach (int el in res)
            {
                Console.Write("{0, 4:d}", el);
            }
            Console.WriteLine();
            Console.ReadKey(false);

            Console.WriteLine("Test 3:");
            Console.WriteLine("Your matrix:");
            mat = Test3();
            MatrixRender(mat);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();
            res = MatrixPositiveColumnSum(mat);
            foreach (int el in res)
            {
                Console.Write("{0, 4:d}", el);
            }
            Console.WriteLine();
            Console.ReadKey(false);

            Console.WriteLine("Test 4:");
            Console.WriteLine("Your matrix:");
            mat = Test4();
            MatrixRender(mat);
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();
            res = MatrixPositiveColumnSum(mat);
            foreach (int el in res)
            {
                Console.Write("{0, 4:d}", el);
            }
            Console.WriteLine();
            Console.ReadKey(false);
        }
    }
}
