﻿/*
    Проект является собственностью разработчика известного как Mister Class.
    Распространение исходных файлов должно происходить только с его разрешения.
    Ибо нечего тырить чужие исходники, надо которыми он старается.
    Попытки стырить исходники карается штрафом, от злобного взгляда до пинка.
    Mister Class © 2019 All rights reserved.
    Task:  Дана строка. Удалить в данной строке символ, стоящий на заданной позиции. 
 */
// <copyright file="Program.cs" company="No">
// Copyright (c) 2019 All Rights Reserved
// </copyright>
// <author>Mister Class</author>
// <date>10/03/2019 23:00:00 </date>
// <summary>Main Program</summary>

using System;
using System.Text;

namespace Lab1_2
{
    class Program
    {
        /// <summary>
        /// Removing one symbol from string
        /// </summary>
        /// <param name="builder">StringBuilder for handling</param>
        /// <param name="index">Index symbol to be removed</param>
        /// <returns></returns>
        public static string SymbRemoveFromString(StringBuilder builder, int index)
        {
            if (builder != null && index >= 0 && index < builder.Length)
            {
                builder.Remove(index, 1);
                return builder.ToString();
            }
            return null;
        }

        public static string Test1(out int ind)
        {
            ind = 0;
            return null;
        }
        public static string Test2(out int ind)
        {
            ind = 0;
            return "f";
        }
        public static string Test3(out int ind)
        {
            ind = -1;
            return "f";
        }
        public static string Test4(out int ind)
        {
            ind = 0;
            return "Test of testing";
        }
        public static string Test5(out int ind)
        {
            ind = 14;
            return "Test of testing";
        }
        public static string Test6(out int ind)
        {
            ind = 9;
            return "Test of testing";
        }
        public static string Test7(out int ind)
        {
            ind = 33;
            return "Test of testing";
        }
        static void Main(string[] args)
        {
            string str = Test1(out int ind);
            StringBuilder builder = new StringBuilder(str);
            Console.WriteLine("Your string is " + str);
            Console.WriteLine("Your index is " + ind);
            string res = SymbRemoveFromString(builder, ind);
            if (res != null)
            {
                Console.WriteLine("Your result: " + res);
            }
            else Console.WriteLine("Error!");
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            str = Test2(out ind);
            builder = new StringBuilder(str);
            Console.WriteLine("Your string is " + str);
            Console.WriteLine("Your index is " + ind);
            res = SymbRemoveFromString(builder, ind);
            if (res != null)
            {
                Console.WriteLine("Your result: " + res);
            }
            else Console.WriteLine("Error!");
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            str = Test3(out ind);
            builder = new StringBuilder(str);
            Console.WriteLine("Your string is " + str);
            Console.WriteLine("Your index is " + ind);
            res = SymbRemoveFromString(builder, ind);
            if (res != null)
            {
                Console.WriteLine("Your result: " + res);
            }
            else Console.WriteLine("Error!");
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            str = Test4(out ind);
            builder = new StringBuilder(str);
            Console.WriteLine("Your string is " + str);
            Console.WriteLine("Your index is " + ind);
            res = SymbRemoveFromString(builder, ind);
            if (res != null)
            {
                Console.WriteLine("Your result: " + res);
            }
            else Console.WriteLine("Error!");
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            str = Test5(out ind);
            builder = new StringBuilder(str);
            Console.WriteLine("Your string is " + str);
            Console.WriteLine("Your index is " + ind);
            res = SymbRemoveFromString(builder, ind);
            if (res != null)
            {
                Console.WriteLine("Your result: " + res);
            }
            else Console.WriteLine("Error!");
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            str = Test6(out ind);
            builder = new StringBuilder(str);
            Console.WriteLine("Your string is " + str);
            Console.WriteLine("Your index is " + ind);
            res = SymbRemoveFromString(builder, ind);
            if (res != null)
            {
                Console.WriteLine("Your result: " + res);
            }
            else Console.WriteLine("Error!");
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();

            str = Test7(out ind);
            builder = new StringBuilder(str);
            Console.WriteLine("Your string is " + str);
            Console.WriteLine("Your index is " + ind);
            res = SymbRemoveFromString(builder, ind);
            if (res != null)
            {
                Console.WriteLine("Your result: " + res);
            }
            else Console.WriteLine("Error!");
            Console.WriteLine("Press any key to go on...");
            Console.ReadKey();
        }
    }
}
